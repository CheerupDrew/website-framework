from django.shortcuts import render, redirect
from django.contrib import messages

import bcrypt
from .models import *




def index(request):
    if request == 'POST':
        return redirect ('/')
    else:
        return render (request, 'index.html')

def register(request):
        errors = User.objects.user_validator(request.POST)
        if len(errors):
            for key, value in errors.items():
                messages.error(request,value)
            return redirect('/')
        else:
            password = request.POST['password']
            pw_hash = bcrypt.hashpw(password.encode(), bcrypt.gensalt()).decode()
            add_user = User.objects.create(first_name = request.POST['first_name'], last_name = request.POST['last_name'], email = request.POST['email'], password = pw_hash)
            request.session['user_id'] = add_user.id
            return redirect('/wishes')

def success(request):
    return render(request, 'success.html')


def login(request):
    email = request.POST['email']
    password = request.POST['password']

    user = User.objects.filter(email = email)

    if len(user) == 0:
        messages.error(request,'Invalid Login')
        return redirect ('/')
    elif bcrypt.checkpw(password.encode(), user[0].password.encode()):
        request.session['user_id'] = user[0].id
        return redirect ('/wishes')
    return redirect ('/')

def wishes(request):
    if request.session['user_id']:
        context = {
            'wishes': User.objects.get(id = request.session['user_id']).wishes.all(),
            'granted_wishes': Granted_wish.objects.all(),
            'user' : User.objects.get(id = request.session['user_id'])
        }
    return render(request,'wishes.html', context)
def new(request):
    if request.session['user_id']:
        context = {
            'user': User.objects.get(id = request.session['user_id'])
        }
        return render(request, 'new_wish.html', context)
    else:
        return redirect ('/')

def edit(request, wish_id):
    edit = Wish.objects.get(id=wish_id)
    context = {
        'user': User.objects.get(id = request.session['user_id']),
        'wish': edit
    }
    return render(request, 'edit_wish.html', context)

def update(request, wish_id):
    errors = Wish.objects.wish_validator(request.POST)
    if len(errors):
        for key, value in errors.items():
            messages.error(request, value)
            return redirect('/edit/' + str(wish_id))
    else:
        wish = Wish.objects.get(id= wish_id)
        wish.item = request.POST['item']
        wish.desc = request.POST['desc']
        wish.save()
        return redirect('/wishes')    
    return redirect('/')

def logout(request):
    request.session.clear()
    return redirect ('/')

def new_wish(request):
    errors = Wish.objects.wish_validator(request.POST)
    if len(errors):
        for key, value in errors.items():
            messages.error(request,value)
        return redirect('/new')
    else:
        item = request.POST['item']
        desc = request.POST['desc']
        user = User.objects.get(id = request.session['user_id'])
        add_wish = Wish.objects.create(item = item, desc = desc, user = user)
        return redirect('/wishes')
    return errors
def grant(request, wish_id):
    if request.method == 'POST':
        Granted_wish.objects.create(item=request.POST['wish_item'], user=User.objects.get(id=request.POST['user_id']), date_added=request.POST['wish_created'])
        Wish.objects.get(id=wish_id).delete()
        return redirect('/wishes')
    else:
        return redirect('/')

def delete(request):
    if request.method == 'POST':
        wish = Wish.objects.get(id=request.POST['wish_id'])
        wish.delete()
        return redirect('/wishes')
    else:
        return redirect('/')