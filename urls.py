from django.urls import path
from . import views

urlpatterns = [
    path('', views.index),
    path('register/', views.register),
    path('login/', views.login),
    path('success/', views.success),
    path('wishes/', views.wishes),
    path('edit/<wish_id>/', views.edit),
    path('new/', views.new),
    path('logout/', views.logout),
    path('new_wish/', views.new_wish),
    path('grant/<wish_id>/', views.grant),
    path('update_wish/<wish_id>/', views.update),
    path('delete/', views.delete),
]