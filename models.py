from django.db import models
from django.contrib import messages

class UserManager(models.Manager):
    def user_validator(self,post_data):
        errors = {}

        if len(post_data['first_name']) == 0:
            errors['first_name'] = 'First name must not be empty'
        
        if len(post_data['last_name']) == 0:
            errors['last_name'] = 'Last name must not be empty'
        
        if len(post_data['password']) == 0:
            errors['password'] = "Password must not be empty"
        
        if post_data['password']!= post_data['confirm_password']:
            errors['password_match'] = 'Passwords must match'
        return errors

class WishManager(models.Manager):
    def wish_validator(self, post_data):
        errors = {}
        if len(post_data['item']) < 3:
            errors['item'] = "Item must be longer than 3 characters."
        if len(post_data['desc']) < 3:
            errors['desc'] = "Description must be longer than 3 characters."
        return errors

class User(models.Model):
    first_name = models.CharField(max_length = 255)
    last_name = models.CharField(max_length = 255)
    email = models.CharField(max_length = 100)
    password = models.CharField(max_length = 10)
    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)
    objects = UserManager()

class Wish(models.Model):
    item = models.CharField(max_length=255)
    desc = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, related_name="wishes", on_delete = models.CASCADE)

    objects = WishManager()

class Granted_wish(models.Model):
    item = models.CharField(max_length=255)
    date_added = models.DateTimeField(auto_now_add= True)
    granted_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, related_name="granted_wishes", on_delete = models.CASCADE)